import React from 'react';

import { useFormContext } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import { TextField as MUITextField } from "@material-ui/core";

type TextFieldProps = {
    name: string,
    id?: string,
    required?: boolean,
    fullWidth?: boolean,
    label: string,
    autoFocus?: boolean,
    maxLength?: number,
    max?: number,
    min?: number,
    type?: React.InputHTMLAttributes<unknown>['type'],
}

function TextField({ name, id, required = false, fullWidth = true, label, autoFocus = false, maxLength = 255, max = 0, min = 0, type = 'text' }: TextFieldProps) {
    const { register, formState: { errors } } = useFormContext();

    const { [name]: error } = errors;

    return (
        <>
            <MUITextField
                variant="outlined"
                fullWidth={fullWidth}
                id={id || name}
                label={label}
                autoFocus={autoFocus}
                error={!!error?.message}
                helperText={<ErrorMessage errors={errors} name={name} />}
                type={type}
                {...register(name, {
                    required: required ? 'This field is required' : false,
                    maxLength: { value: maxLength, message: 'This field exceeds the maximum length allowed', },
                    max: { value: max, message: `This field cannot be more than ${max}` },
                    min: { value: min, message: `This field cannot be less than ${min}` },
                })}
            />
        </>
    );
}

export default TextField;