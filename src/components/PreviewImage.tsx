import React, { Component } from 'react';

interface PreviewImageProps {
    className: string
    url: string
}

class PreviewImage extends Component<PreviewImageProps> {
    onRemove() {

    }
    render() {
        return (
            <img className={this.props.className} alt="vehicle preview"  src={this.props.url} />
        );
    }
}

export default PreviewImage;