import React from 'react';

import { useFormContext } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

interface Option {
    id: string | number,
    label: string,
}

type SelectFieldProps = {
    name: string,
    id?: string,
    required?: boolean,
    label: string,
    autoFocus?: boolean,
    options: Option[],
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            minWidth: '100%',
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    }),
);

function SelectField({ name, id, label, options, required = false, autoFocus = false }: SelectFieldProps) {
    const classes = useStyles();
    const { register, formState: { errors } } = useFormContext();

    const { [name]: error } = errors;

    return (
        <>
            <FormControl error={!!error?.message} className={classes.formControl}>
                <InputLabel id={id || name}>{label}</InputLabel>
                <Select
                    labelId={id || name}
                    id={id || name}
                    displayEmpty
                    className={classes.selectEmpty}
                    {...register(name, {
                        required: required ? 'This field is required' : false,
                    })}
                >
                    {options.map((option) =>
                        <MenuItem key={option.id} value={option.id}>{option.label}</MenuItem>
                    )}
                </Select>
                <FormHelperText>
                    <ErrorMessage errors={errors} name={name} />
                </FormHelperText>
            </FormControl>
        </>
    );
}

export default SelectField;