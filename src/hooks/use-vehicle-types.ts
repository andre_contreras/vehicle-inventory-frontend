import {useEffect, useState} from "react";
import axios from 'axios';

import useAuth from "./use-auth";
import useSnackbar from "./use-snackbar";

export interface VehicleType {
    id: number
    name: string
    wheel_number: number
}

let vehicleTypesRetrievalTimeout: NodeJS.Timeout;

function useVehicleTypes() {
    const { axiosClient } = useAuth();
    const { open } = useSnackbar();
    const [vehicleTypes, setVehicleTypes] = useState<VehicleType[] | null>(null);

    useEffect(() => {
        let ignore = false;
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        if (vehicleTypes === null) {
            clearTimeout(vehicleTypesRetrievalTimeout);

            vehicleTypesRetrievalTimeout = setTimeout(() => {
            axiosClient.get('/api/v1/vehicle-type', {
                cancelToken: source.token,
            }).then(({ data }) => {
                if (!ignore) {
                    setVehicleTypes(data.vehicles_types);
                }
            }).catch((e) => {
                if (!axios.isCancel(e)) {
                    open('Server error retrieving vehicle types', 'error');
                    setVehicleTypes([]);
                }
            });
            }, 400);
        }

        return () => {
            ignore = true;
            source.cancel('Operation canceled by the user.');
        }
    }, [axiosClient, vehicleTypes, open]);

    return { vehicleTypes };
}

export default useVehicleTypes;
