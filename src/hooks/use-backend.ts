
const backendValues = {
    host: 'http://localhost:43985'
};

function useBackend() {
    const { host } = backendValues;
    return {
        host,
    }
}

export default useBackend;
