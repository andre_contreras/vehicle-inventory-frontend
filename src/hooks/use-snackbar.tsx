import React, {useCallback, useEffect, useMemo, useState} from 'react';
import ReactDOM from "react-dom";

import Snackbar from "@material-ui/core/Snackbar";
import Alert, { Color } from '@material-ui/lab/Alert';

let node: HTMLElement | null = null

function useSnackbar() {
    const [open, setOpen] = useState(false);
    const [message, setMessage] = useState('');
    const [type, setType] = useState<Color>('success');

    const handleClose = useCallback(() => {
        setOpen(false);
    }, []);

    const openSnackbar = useCallback((message: string, type: Color) => {
        setMessage(message);
        setType(type);
        setOpen(true);
    }, []);

    const snackbarContainer = useMemo(() => (
        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity={type} elevation={6} variant="filled">
                {message}
            </Alert>
        </Snackbar>
    ), [handleClose, message, open, type]);

    useEffect(()=>{
        node && ReactDOM.render(snackbarContainer,node)
    }, [snackbarContainer]);

    useEffect(()=>{
        if (!node) {
            node = document.createElement('div');
            document.body.appendChild(node);
        }
    },[snackbarContainer]);

    return {
        snackbarContainer,
        open: openSnackbar,
    }
}

export default useSnackbar;