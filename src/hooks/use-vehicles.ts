import {useEffect, useState} from "react";
import axios from 'axios';

import useAuth from "./use-auth";
import { VehicleType } from "./use-vehicle-types";
import useSnackbar from "./use-snackbar";

interface Image {
    url: string
}

interface Time {
    date: string
    timezone_type: number
    timezone: string
}

interface Vehicle {
    id: number
    model: string
    year: number
    horse_power: number
    type: VehicleType
    images: Image[]
    created: Time
}

let vehiclesRetrievalTimeout: NodeJS.Timeout;

function useVehicles() {
    const { axiosClient } = useAuth();
    const { open } = useSnackbar();
    const [vehicles, setVehicles] = useState<Vehicle[] | null>(null);
    const [vehiclesCount, setVehiclesCount] = useState(null);

    useEffect(() => {
        let ignore = false;
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        if (vehicles === null) {
            clearTimeout(vehiclesRetrievalTimeout);

            vehiclesRetrievalTimeout = setTimeout(() => {
            axiosClient.get('/api/v1/vehicle', {
                cancelToken: source.token,
            }).then(({ data }) => {
                if (!ignore) {
                    open('Vehicles retrieved successfully', 'success');
                    setVehicles(data.vehicles);
                    setVehiclesCount(data.meta.count);
                }
            }).catch((e) => {
                if (!axios.isCancel(e)) {
                    open('Server error retrieving vehicles', 'error');
                    setVehicles([]);
                }
            });
            }, 400);
        }

        return () => {
            ignore = true;
            source.cancel('Operation canceled by the user.');
        }
    }, [axiosClient, vehicles, open]);

    return { vehicles };
}

export default useVehicles;
