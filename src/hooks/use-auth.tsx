import React, {useCallback, useMemo, useState} from 'react';
import axios, { AxiosRequestConfig } from 'axios';
import { useJwt } from "react-jwt";

import useBackend from "./use-backend";

interface setTokenProps {
    token: string
    refreshToken: string
}

interface DecodedJWTToken extends Object{
    username: string
}

function useAuth() {
    const [token, setToken] = useState<string>(() => localStorage.getItem('token') ?? '');
    const [refreshToken, setRefreshToken] = useState<string>(() => localStorage.getItem('refreshToken') ?? '');
    const { isExpired, reEvaluateToken, decodedToken }: any = useJwt(token);
    const { host } = useBackend();
    
    const internalSetToken = useCallback((response: setTokenProps) => {
        const { token: responseToken, refreshToken: responseRefreshToken } = response;
        if (!responseToken || !responseRefreshToken) {
            console.error('no token or no refresh token', response);
        }
        localStorage.setItem('token', responseToken);
        localStorage.setItem('refreshToken', responseRefreshToken);
        setToken(responseToken);
        setRefreshToken(responseRefreshToken);

        reEvaluateToken(responseToken);
    }, [reEvaluateToken]);
    
    const axiosClient = useMemo(() => {
        const axiosConfig: AxiosRequestConfig = {
            baseURL: host,
        };
        if (token) {
            axiosConfig.headers = {
                Authorization: `Bearer ${token}`
            };
        }
        const client = axios.create(axiosConfig);

        client.interceptors.response.use((response) => {
            return response;
        }, function onAxiosError(error) {
            const originalRequest = error.config;
            if (error.response.status === 401 && !originalRequest._retry) {

                originalRequest._retry = true;
                return client.post('/api/token/refresh',
                    {
                        "username": decodedToken.username,
                        "refreshToken": refreshToken,
                    }, {
                        headers: {
                            Authorization: undefined,
                        }
                    })
                    .then(res => {
                        if (res.status === 200) {
                            // 1) put token to LocalStorage
                            internalSetToken(res.data);

                            // 2) Change Authorization header
                            axios.defaults.headers.common['Authorization'] = `Bearer ${res.data.token}`;

                            // 3) return originalRequest object with Axios.
                            return axios(originalRequest);
                        }
                    })
            }
        });

        return client;
    }, [decodedToken, host, internalSetToken, refreshToken, token]);

    // @ts-ignore
    const isAuthenticated = token.length > 0;

    const login = useCallback(async (formData) => {
        const response = await axiosClient({
            method: 'post',
            url: '/api/login_check',
            data: formData,
            headers: { "Content-Type": "application/json" },
        }).catch((e) => {
            return e;
        });
        
        if (response.data.token) {
            internalSetToken(response.data);
        }

        return response;
    }, [axiosClient, internalSetToken]);

    return { isAuthenticated, tokenIsExpired: isExpired, reEvaluateToken, login, axiosClient, decodedToken };
}

export default useAuth;