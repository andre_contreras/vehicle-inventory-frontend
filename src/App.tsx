import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import Homepage from './pages/index';
import LoginPage from './pages/login';
import CreateCarPage from './pages/create-vehicle';

import './App.css';

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/" exact>
                    <Homepage/>
                </Route>
                <Route path="/login">
                    <LoginPage/>
                </Route>
                <Route path="/create-car">
                    <CreateCarPage/>
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
