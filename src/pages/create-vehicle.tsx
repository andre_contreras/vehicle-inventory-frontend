import React, {useCallback, useMemo, useState} from 'react';

import { useForm, FormProvider } from "react-hook-form";
import { useHistory } from 'react-router-dom';

import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from "@material-ui/core/Typography";
import Backdrop from '@material-ui/core/Backdrop';
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';

import TextField from "../components/TextField";
import SelectField from "../components/SelectField";

import Layout from '../layout';

import useVehicleTypes from "../hooks/use-vehicle-types";
import useAuth from "../hooks/use-auth";

import PreviewImage from "../components/PreviewImage";
import useSnackbar from "../hooks/use-snackbar";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        previewImage: {
            width: '100%',
            height: 'auto',
            // margin: 15,
        }
    }),
);

function CreateCarPage() {
    const classes = useStyles();
    const { vehicleTypes } = useVehicleTypes();
    const formMethods = useForm();
    const [images, setImages] = useState<any>([]);
    const { axiosClient } = useAuth();
    const { open } = useSnackbar();
    const { push } = useHistory();

    const onSubmit = useCallback(async (data) => {
        console.log(data);
        const formData = new FormData();
        Object.keys(data).forEach((field) => {
            formData.append(field, data[field]);
        });

        images.forEach((image: any) => {
            formData.append('images[]', image);
        });

        const response = await axiosClient({
            method: 'post',
            url: '/api/v1/vehicle',
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            },
        }).catch((e) => {
            return null;
        });

        if (response) {
            open(response.data.message, 'success');
            push('/');
        } else {
            open('There was an error. Try again later', 'error');
        }
    }, [axiosClient, images, open, push]);
    
    const categories = useMemo(() => {
        if (!vehicleTypes) return [];
        
        return vehicleTypes.map((vehicleType) => ({
            id: vehicleType.id,
            label: vehicleType.name,
        }));
    }, [vehicleTypes]);

    const onImageAdd = useCallback((e) => {
        const newImages = [
            ...images,
            e.target.files[0],
        ];
        setImages(newImages);
    }, [images]);

    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const maximumAllowedYear = currentYear + 1;
    const minimumAllowedYear = currentYear - 100;

    return (
        <Layout needsAuth needsRol={['admin']}>
            <>
                {!vehicleTypes && (
                    <Backdrop open>
                        <CircularProgress color="inherit" />
                    </Backdrop>
                )}
                {vehicleTypes && (
                    <FormProvider {...formMethods} >
                        <form onSubmit={formMethods.handleSubmit(onSubmit)}>
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <Typography>Form to create a new Vehicle. Please complete the required fields</Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <SelectField name="category" label="Category" options={categories} />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField name="model" required fullWidth label="Model" autoFocus />
                                </Grid>
                                <Grid item xs={12} sm={3}>
                                    <TextField name="year" required fullWidth label="Year" type="number" max={maximumAllowedYear} min={minimumAllowedYear} />
                                </Grid>
                                <Grid item xs={12} sm={3}>
                                    <TextField name="horse_power" required fullWidth label="Horse Power" type="number" max={1000} min={50} />
                                </Grid>
                                <Grid item xs={12}>
                                    <Button
                                        variant="contained"
                                        component="label"
                                    >
                                        Upload an image
                                        <input
                                            type="file"
                                            hidden
                                            onChange={onImageAdd}
                                        />
                                    </Button>
                                </Grid>
                                <Grid item xs={12}>
                                    <Grid container spacing={3}>
                                        {images && images.map((image: any) => (
                                            <Grid item xs={12} sm={2} key={image}>
                                                <PreviewImage className={classes.previewImage} url={URL.createObjectURL(image)} />
                                            </Grid>
                                        ))}
                                    </Grid>
                                </Grid>
                                <Grid item xs={12}>
                                    <Button type="submit" variant="contained" color="primary">Submit</Button>
                                </Grid>
                            </Grid>
                        </form>
                    </FormProvider>
                )}
            </>
        </Layout>
    );
}

export default CreateCarPage;