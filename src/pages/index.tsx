import React, {useCallback} from 'react';
import { useHistory } from 'react-router-dom';

import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Backdrop from "@material-ui/core/Backdrop";
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import Layout from '../layout';

import Card from "../components/Card";

import useVehicles from "../hooks/use-vehicles";
import useBackend from "../hooks/use-backend";

const useStyles = makeStyles((theme) => ({
    fabWrapper: {
        position: 'absolute',
        bottom: 15,
        right: 35,
    }
}));

function Homepage() {
    const classes = useStyles();
    const {vehicles} = useVehicles();
    const {host} = useBackend();
    const { push } = useHistory();
    
    const onAddClick = useCallback(() => {
        push('/create-car');
    }, [push]);

    return (
        <Layout needsAuth needsRol={['user']}>
            <Grid container spacing={3}>
                {!vehicles && (
                    <Backdrop open>
                        <CircularProgress color="inherit" />
                    </Backdrop>
                )}
                {vehicles && vehicles.map((vehicle) => (
                    <Grid item xs={12} lg={4}>
                        <Card key={vehicle.id} title={vehicle.model} subtitle={vehicle.year}
                              imagePath={`${host}/${vehicle.images?.[0]?.url}`} content={
                                <>
                                    <Typography>
                                        Category: {vehicle.type.name}
                                    </Typography>
                                    <Typography>
                                        Horse power: {vehicle.horse_power}
                                    </Typography>
                                </>
                            } />
                    </Grid>
                ))}
            </Grid>

            <div className={classes.fabWrapper}>
                <Fab color="primary" aria-label="add" onClick={onAddClick}>
                    <AddIcon />
                </Fab>
            </div>
        </Layout>
    );
}

export default Homepage;