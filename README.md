# Quick start
## Requirements
- Node.js v14+
- NPM
- Yarn (preferably)
- Backend running and port at hand

## Instructions
- `yarn install` or `npm install`
- Modify `src/hooks/use-backend.ts` `backendValues.host` with correct port
- `yarn start` or `npm start`